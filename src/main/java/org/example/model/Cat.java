package org.example.model;


public class Cat {

    private long id;

    private String name;

    @Override
    public String toString() {
        return "Cat{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Cat(long id, String name) {
        this.id = id;
        this.name = name;
    }

    protected Cat() {
    }


}
